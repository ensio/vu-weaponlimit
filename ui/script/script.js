function setWeaponLimits(weaponLimits) {
	if (!weaponLimits) {
		return;
	}

	emptyWeaponList();
	
	const sortedKeys = Object.keys(weaponLimits).sort(function(a,b){return weaponLimits[a].order - weaponLimits[b].order});
	
	// create an element for each weaponlimit in the array.
	sortedKeys.forEach(weaponLimitType => {
		const weaponLimit = weaponLimits[weaponLimitType];
		const slotClass = weaponLimit.playerHasWeaponSlot ? "slotActive" :
							weaponLimit.amount >= weaponLimit.limit ? "limitReached" : null;

		var li = document.createElement("li");

		li.classList.add("weaponLimit");

		if (slotClass) {
			li.classList.add(slotClass);
		}

		li.innerHTML = `<label>${weaponLimitType}: ${weaponLimit.amount}/${weaponLimit.limit}</label>`;

		let hoverInfoContent = "";
		Object.keys(weaponLimit.playerNames).sort().forEach(playerName => hoverInfoContent+= `<label>${playerName}</label>`)
		
		if (hoverInfoContent != "") {
			li.innerHTML += `<div class="hover-info" style="display: none">${hoverInfoContent}</div>`;

			li.addEventListener('mouseover', function(e) { 
				li.querySelector(".hover-info").style.display = "";
			});

			li.addEventListener('mouseleave', function(e) { 
				li.querySelector(".hover-info").style.display = "none";
			});
		}

		// Attach it to the weaponLimitList element.
		const el = document.getElementById("weaponLimitList"); 
		el.append(li);
	});	
}

// Clears all player info elements.
function emptyWeaponList(){
	let el = document.getElementById("weaponLimitList"); 
	el.innerHTML = '';
}

fontsize = function () {
	// As the vanilla UI stops resizing at 720p we can't use percentages or vmin for font size, so we calculate it with the width of the orange rectangle.
	// Check style.css to see how the UI mimics the fixed aspect ratio of the vanilla UI.
	let el = document.getElementById("title"); 
	let size = el.clientWidth * 0.06;

	let pageEl = document.getElementById("container"); 
	pageEl.style.fontSize = size + 'px';
};


window.onresize = function(event) {
	fontsize();
};

window.onload = function(event) {
	fontsize();
};