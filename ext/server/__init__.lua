local weaponLimits = {
    ["Sniper"] = {
        order = 1,
        limit = 5,
        weapons = {
            ["Weapons/XP1_L96/L96"] = true,
            ["Weapons/M40A5/M40A5"] = true,
            ["Weapons/Model98B/Model_98B"] = true,
            ["Weapons/XP2_JNG90/JNG90"] = true,
            ["Weapons/SV98/SV98"] = true
        }
    },
    ["DMR"] = {
        order = 2,
        limit = 5,
        weapons = {
            ["Weapons/MK11/Mk11"] = true,
            ["Weapons/SVD/SVD"] = true,
            ["Weapons/XP1_QBU-88/QBU-88"] = true,
            ["Weapons/M39EBR/M39EBR"] = true,
            ["Weapons/SKS/SKS"] = true,
            ["Weapons/XP2_HK417/HK417"] = true, -- M417
        }
    }
}

function getTeamWeaponSlotAmount(weaponTypeName, teamId)
    local teamWeaponAmount = 0
    for playerName in pairs(teamStatus[teamId][weaponTypeName]) do
        if teamStatus[teamId][weaponTypeName][playerName] == true then
            teamWeaponAmount = teamWeaponAmount + 1
        end
    end
    return teamWeaponAmount
end

function broadcastWeaponLimitStatusChange()
    local weaponLimitStatus = {}

    for teamId in pairs(teamStatus) do
        weaponLimitStatus[teamId] = {}

        for weaponTypeName, weaponLimit in pairs(weaponLimits) do
            weaponLimitStatus[teamId][weaponTypeName] = {
                order = weaponLimit.order,
                amount = getTeamWeaponSlotAmount(weaponTypeName, teamId),
                playerNames = teamStatus[teamId][weaponTypeName],
                limit = weaponLimit.limit
            }
        end
    end

    NetEvents:Broadcast('WeaponLimitStatusChange', weaponLimitStatus)
end

function setWeaponSlot(weaponTypeName, player, value, broadcastUpdate)
    setWeaponSlotWithTeam(weaponTypeName, player.teamId, player.name, value, broadcastUpdate)
end

function setWeaponSlotWithTeam(weaponTypeName, teamId, playerName, value, broadcastUpdate)
    if broadcastUpdate == nil then
        broadcastUpdate = true
    end

    local changed = false

    if weaponTypeName == nil then
        for weaponTypeName in pairs(teamStatus[teamId]) do
            if teamStatus[teamId][weaponTypeName][playerName] ~= value then
                changed = true
            end
            teamStatus[teamId][weaponTypeName][playerName] = value
        end
    else
        if teamStatus[teamId][weaponTypeName][playerName] ~= value then
            changed = true
        end
        teamStatus[teamId][weaponTypeName][playerName] = value
    end
    
    if broadcastUpdate and changed then
        broadcastWeaponLimitStatusChange()
    end
end

function initTeamStatus()
    teamStatus = {}
    
    for teamId = 1, 16 -- max 16 teams
    do
        teamStatus[teamId] = {}

        -- each weapon type limit
        for weaponTypeName in pairs(weaponLimits) do
            teamStatus[teamId][weaponTypeName] = {}
        end
    end
end

initTeamStatus()

Events:Subscribe('Player:Authenticated', function(player)
    broadcastWeaponLimitStatusChange()
end)

Events:Subscribe('Player:Left', function(player)
    if player.teamId > 0 then
        setWeaponSlot(nil, player, nil)
    end
end)

Events:Subscribe('Player:TeamChange', function(player, team, squad)
    -- try to remove player from all teams and all weapon types (we dont know the previous team id)
    for teamId in pairs(teamStatus) do
        setWeaponSlotWithTeam(nil, teamId, player.name, nil)
    end
end)

Events:Subscribe('Server:RoundOver', function(roundTime, winningTeam)
    initTeamStatus()
    broadcastWeaponLimitStatusChange()
    NetEvents:Broadcast('WeaponLimitHideUI')
end)

Events:Subscribe('Server:RoundReset', function()
    initTeamStatus()
    broadcastWeaponLimitStatusChange()
    NetEvents:Broadcast('WeaponLimitHideUI')
end)

function getWeaponType(weaponName)
    for weaponTypeName, weaponLimit in pairs(weaponLimits) do
        if weaponLimit.weapons[weaponName] then
            return weaponTypeName
        end
    end

    return nil
end

function hasWeaponSlot(weaponTypeName, player)
    return teamStatus[player.teamId][weaponTypeName][tostring(player.name)]
end

Events:Subscribe('Player:Respawn', function(player)
    local weaponName = player.soldier.weaponsComponent.currentWeapon.name
    local weaponTypeName = getWeaponType(weaponName)

    if weaponTypeName ~= nil then
        local teamWeaponSlotAmount = getTeamWeaponSlotAmount(weaponTypeName, player.teamId)
        local weaponLimit = weaponLimits[weaponTypeName].limit
    
        if hasWeaponSlot(weaponTypeName, player) then
            -- do nothing
        elseif (teamWeaponSlotAmount < weaponLimit) then
            setWeaponSlot(nil, player, nil, false)
            setWeaponSlot(weaponTypeName, player, true)
        else
            RCON:SendCommand("admin.killplayer", { ['name'] = player.name }) -- Kill without counting as death
            ChatManager:Yell(weaponTypeName .. " limit reached! Please spawn with another weapon", 10, player)
        end
    else
        setWeaponSlot(weaponTypeName, player, nil)
    end
end)