weaponLimitStatus = {}

Events:Subscribe('Extension:Loaded', function()
    WebUI:Init()
    WebUI:Hide()
end)

Events:Subscribe('Player:Connected', function(player)
    local localPlayer = PlayerManager:GetLocalPlayer()

    if localPlayer ~= player then
        return
    end

    WebUI:Show()
end)

Events:Subscribe('Player:Respawn', function(player)
    local localPlayer = PlayerManager:GetLocalPlayer()

    if localPlayer ~= player then
        return
    end

    WebUI:Hide()
end)

Events:Subscribe('Player:TeamChange', function(player, team, squad)
    local localPlayer = PlayerManager:GetLocalPlayer()

    if localPlayer ~= player then
        return
    end

    local limitStatus = weaponLimitStatus[player.teamId]

    setHasWeaponSlot(limitStatus, player)

    WebUI:ExecuteJS('setWeaponLimits(' .. json.encode(limitStatus) .. ');')
end)

Events:Subscribe('Level:Destroy', function()
    WebUI:Hide()
end)

Events:Subscribe('Soldier:HealthAction', function(soldier, action)
    local localPlayer = PlayerManager:GetLocalPlayer()
    local localSoldier = localPlayer.soldier or localPlayer.corpse

    if localSoldier ~= soldier then
        return
    end

    if action == HealthStateAction.OnDead then
        WebUI:Show()
    end

    if action == HealthStateAction.OnRevive then
        WebUI:Hide()
    end
end)

NetEvents:Subscribe('WeaponLimitStatusChange', function(newWeaponLimitStatus)
    weaponLimitStatus = newWeaponLimitStatus
    local player = PlayerManager:GetLocalPlayer()

    if player ~= nil and player.teamId > 0 then
        local limitStatus = newWeaponLimitStatus[player.teamId]

        setHasWeaponSlot(limitStatus, player)
    
        WebUI:ExecuteJS('setWeaponLimits(' .. json.encode(limitStatus) .. ');')
    end
end)

function setHasWeaponSlot(limitStatus, player)
    if limitStatus then
        for _, weaponLimit in pairs(limitStatus) do
            weaponLimit.playerHasWeaponSlot = weaponLimit.playerNames[player.name] == true
        end
    end
end

NetEvents:Subscribe('WeaponLimitHideUI', function()
    WebUI:Hide()
end)